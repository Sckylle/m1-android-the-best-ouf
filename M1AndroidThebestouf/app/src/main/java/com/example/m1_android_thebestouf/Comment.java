package com.example.m1_android_thebestouf;

import android.os.Parcel;
import android.os.Parcelable;

public class Comment implements Parcelable {

    private User user;
    private int Id_video;
    private String text;

    public Comment(User user, int id_video, String text) {
        this.user = user;
        Id_video = id_video;
        this.text = text;
    }

    protected Comment(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        Id_video = in.readInt();
        text = in.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeInt(Id_video);
        dest.writeString(text);
    }

    public User getUser() {
        return user;
    }

    public int getId_video() {
        return Id_video;
    }

    public String getText() {
        return text;
    }
}
