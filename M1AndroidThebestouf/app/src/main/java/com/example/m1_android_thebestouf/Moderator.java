package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Moderator extends AppCompatActivity {

    private Button selectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_moderator);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        selectButton = findViewById(R.id.moderator);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    public void head(View view) {
        Header.head(view, this);
    }

    public void signalementPage(View view) {
        Intent intent = new Intent(this, ModeratorSignalement.class);
        startActivity(intent);
    }

    public void gestionContributorPage(View view) {
        Intent intent = new Intent(this, ModeratorGestionContributor.class);
        startActivity(intent);
    }

    public void contentValidation(View view) {
        Intent intent = new Intent(this, Contributor.class);
        startActivity(intent);
    }

}
