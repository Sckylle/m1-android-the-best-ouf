package com.example.m1_android_thebestouf;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class Video implements Parcelable {

    private int Id_video;
    private User user;
    private Defi defi;
    private String uri;
    private int nb_Likes;
    private int nb_Dislikes;

    public Video(User user, Defi defi, String uri, int Id_video) {
        this.user = user;
        this.defi = defi;
        this.uri = uri;
        this.Id_video = Id_video;
    }

    public Video(User user, Defi defi, String uri, int nb_Likes, int nb_Dislikes, int Id_video) {
        this.user = user;
        this.defi = defi;
        this.uri = uri;
        this.nb_Likes = nb_Likes;
        this.nb_Dislikes = nb_Dislikes;
        this.Id_video = Id_video;
    }

    protected Video(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        defi = in.readParcelable(Defi.class.getClassLoader());
        uri = in.readString();
        nb_Likes = in.readInt();
        nb_Dislikes = in.readInt();
        Id_video = in.readInt();
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    public User getUser() {
        return user;
    }

    public Defi getDefi() {
        return defi;
    }

    public String getUri() {
        return uri;
    }

    public int getNb_Likes() {
        return nb_Likes;
    }

    public void setNb_Likes(int nb_Likes) {
        this.nb_Likes = nb_Likes;
    }

    public int getNb_Dislikes() {
        return nb_Dislikes;
    }

    public void setNb_Dislikes(int nb_Dislikes) {
        this.nb_Dislikes = nb_Dislikes;
    }

    public int getId_video() {
        return Id_video;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeParcelable(defi, flags);
        dest.writeString(uri);
        dest.writeInt(nb_Likes);
        dest.writeInt(nb_Dislikes);
        dest.writeInt(Id_video);
    }
}
