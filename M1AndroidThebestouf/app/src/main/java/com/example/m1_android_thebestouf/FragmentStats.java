package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

public class FragmentStats extends Fragment {

    private TextView nbChallengesPosted, nbVideosPosted, nbVideosLiked, nbVideosDisliked, nbFollowers, nbFollowing;
    private int challPost, vidPost, vidLiked, vidDisliked, follow, following;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stats, container, false);

        nbChallengesPosted = rootView.findViewById(R.id.nbChallengesPosted);
        nbVideosPosted = rootView.findViewById(R.id.nbVideosPosted);
        nbVideosLiked = rootView.findViewById(R.id.nbVideosLiked);
        nbVideosDisliked = rootView.findViewById(R.id.nbVideosDisliked);
        nbFollowers = rootView.findViewById(R.id.nbFollowers);
        nbFollowing = rootView.findViewById(R.id.nbFollowing);

        Bundle bundle = getArguments();

        if (bundle != null) {
            User user = bundle.getParcelable("user");
            getStats (user.getId_user());
        } else {
            getStats(UserData.getInstance(getContext()).getId_user());
        }



        return rootView;
    }



    /*
     * Retourne une liste de vidéos de la BDD
     * param context Le contexte
     * param order L'ordre des vidéos
     * param Id_user l'Id_user de l'utilisateur
     */
    public void getStats (int Id_user) {

        RequestParams params = new RequestParams();
        params.put("Id_user", Id_user);
        BaseAdapter.post(Constantes.URL_GETSTATS,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                try {
                    challPost = response.getInt("nb_challenges");
                    vidPost = response.getInt("nb_videos");
                    vidLiked = response.getInt("nb_likes");
                    vidDisliked = response.getInt("nb_dislikes");
                    follow = response.getInt("nb_followers");
                    following = response.getInt("nb_following");

                    nbChallengesPosted.setText(getString(R.string.nbChallengesPosted) + " : " + challPost);
                    nbVideosPosted.setText(getString(R.string.nbVideosPosted) + " : " + vidPost);
                    nbVideosLiked.setText(getString(R.string.nbVideosLiked) + " : " + vidLiked);
                    nbVideosDisliked.setText(getString(R.string.nbVideosDisliked) + " : " + vidDisliked);
                    nbFollowers.setText(getString(R.string.followers) + " : " + follow);
                    nbFollowing.setText(getString(R.string.following) + " : " + following);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {

            }
        });
    }
}
