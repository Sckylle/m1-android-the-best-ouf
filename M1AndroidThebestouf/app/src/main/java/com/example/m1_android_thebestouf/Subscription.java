package com.example.m1_android_thebestouf;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class Subscription extends AppCompatActivity {

    private User user;
    private ViewPager2 viewPager2;
    private TabLayout tabLayout;
    private Button selectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        selectButton = findViewById(R.id.account);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user = bundle.getParcelable("user");
            assert user != null;

        }

        viewPager2 = findViewById(R.id.view_pager);
        ViewAdapterSubscription viewAdapter = new ViewAdapterSubscription(this);
        viewAdapter.receiveUser(user);
        viewPager2.setAdapter(viewAdapter);


        tabLayout = findViewById(R.id.tabLayout);
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.OnConfigureTabCallback() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0:
                        tab.setText(getString(R.string.following));
                        break;
                    case 1:
                        tab.setText(getString(R.string.followers));
                        break;
                }
            }
        });
        tabLayoutMediator.attach();
    }

    public void head(View view) {
        Header.head(view, this);
    }
}
