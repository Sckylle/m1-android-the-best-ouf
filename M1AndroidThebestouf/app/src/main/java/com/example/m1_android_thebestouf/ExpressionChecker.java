package com.example.m1_android_thebestouf;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Sert à vérifier des chaînes de caractères via des expressions régulières
 */
public class ExpressionChecker {


    /*
     * Vérifie si la chaîne passée en paramètre correspond au pattern
     * param p Le pattern
     * param chaine La chaîne à vérifier
     * return true si la chaîne correspond au pattern, false sinon
     */
    private boolean check(String p, String chaine, boolean caseSensitive) {
        Pattern pattern = caseSensitive ? Pattern.compile(p) : Pattern.compile(p, Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(chaine);
        return m.matches();
    }

    /*
     * Vérifie si la chaîne passée en paramètre est une adresse mail
     * param email La chaîne à vérifier
     * return true si la chaîne est une adresse mail, false sinon
     */
    public boolean checkEmail(String email) {
        String p = "(\\w|\\.|\\+|\\-)+@\\w+\\.[a-z]{2,4}";  //Modifiable pour obtenir un autre pattern
        return check(p, email, false);
    }

    /*
     * Vérifie si le mot de passe passé en paramètre est assez sécurisé
     * param password La chaîne à vérifier
     * return true si le mot de passe est assez sécurisé, false sinon
     */
    public boolean checkPassword(String password) {
        String p = "\\w{8,}";  //Modifiable pour obtenir un autre pattern
        return check(p, password, true);
    }

    /*
     * Vérifie si le mot de passe et le mot de passe de confirmation passés en paramètre sont les mêmes
     * param password Le mot de passe
     * param confirmPassword Le mot de passe de confirmation
     * return true si les mot de passes sont identiques, false sinon
     */
    public boolean checkConfirmPassword(String password, String confirmPassword) {
        return check(password, confirmPassword, true);
    }

    /*
     * Vérifie si l'username passé en paramètre est valide
     * param username La chaîne à vérifier
     * return true si l'username est valide, false sinon
     */
    public boolean checkUsername(String username) {
        String p = "\\w{6,}";  //Modifiable pour obtenir un autre pattern
        return check(p, username, false);
    }

    /*
     * Vérifie si la chaîne passée en paramètre ne contient que des chiffres
     * param chaine La chaîne à vérifier
     * return true si la chaîne est valide, false sinon
     */
    public boolean isInteger(String chaine) {
        String p = "\\d+";  //Modifiable pour obtenir un autre pattern
        return check(p, chaine, false);
    }

    public boolean checkRegistration(String username, String password, String email) {
        return (checkUsername(username) && checkPassword(password) && checkEmail(email));
    }

}
