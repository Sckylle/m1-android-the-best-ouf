package com.example.m1_android_thebestouf;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentDefis  extends Fragment {

    private List<Defi> listeDefis = new ArrayList<Defi>();
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_defis, container, false);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), listeDefis);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Bundle bundle = getArguments();

        if (bundle != null) {
           User user = bundle.getParcelable("user");
            receiveDefis (user.getId_user());
        } else {
            receiveDefis(UserData.getInstance(getContext()).getId_user());
        }

        RecyclerView.ItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
        return rootView;
    }

    /*
     * Récupère tous les défis postés par un utilisateur
     * param username Le nom de l'utilisateur
     * return Un tableau contenant les défis de l'utilisateur
     */
    public List<Defi> receiveDefis ( final int Id_user) {


        StringRequest jsonArrayRequest = new StringRequest (Request.Method.POST,
                Constantes.URL_GETCHALLENGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        DefiAdapter dA = new DefiAdapter(getContext());
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")) {
                                JSONArray jsonArray= obj.getJSONArray("challenges");
                                User user;
                                for(int i = 0; i < jsonArray.length(); i++){
                                    Defi defi = dA.parseDefi(jsonArray.getJSONObject(i));
                                    listeDefis.add(defi);
                                    recyclerViewAdapter.notifyDataSetChanged();
                                }

                            } else {
                                Toast.makeText(getContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("order", "DESC");
                params.put("Id_user", String.valueOf(Id_user));
                return params;
            }
        };
        RequestHandler.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);
        return listeDefis;
    }
}