package com.example.m1_android_thebestouf;

import android.content.Context;
import android.content.SharedPreferences;


/*
 * Gère un utilisateur
 * Contient les informations de base du compte
 */
public class UserData {
    private static final String SHARED_PREF = "userData";
    private static final String KEY_ID = "Id_user";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_AGE = "age";
    private static final String KEY_ROLE = "role";
    private static UserData instance;
    private static Context ctx;


    private UserData(Context context) {
        ctx = context;
    }

    public static synchronized UserData getInstance(Context context) {
        if (instance == null) {
            instance = new UserData(context);
        }
        return instance;
    }

    /*
     * Enregistre les informations de l'utilisateur
     */
    public boolean userLogin(int id, String username, String email, int age, String role) {

        SharedPreferences userData = ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userData.edit();

        editor.putInt(KEY_ID, id);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_EMAIL, email);
        editor.putInt(KEY_AGE, age);
        editor.putString(KEY_ROLE, role);

        editor.apply();

        return true;
    }

    /*
     * Renvoie true si l'utilisateur est connecté
     */
    public boolean isLoggedIn() {
        SharedPreferences user = ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);

        return user.getString(KEY_USERNAME, null) != null;
    }

    /*
     * Déconnecte l'utilisateur
     */
    public boolean logout() {
        SharedPreferences user = ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = user.edit();

        editor.clear();
        editor.apply();
        return true;
    }

    /*
     * Retourne l'Id_user
     */
    public int getId_user() {
        return ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getInt(KEY_ID, 0);
    }

    /*
     * Retourne l'username
     */
    public String getUsername() {
        return ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getString(KEY_USERNAME, null);
    }

    /*
     * Retourne l'email
     */
    public String getEmail() {
        return ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getString(KEY_EMAIL, null);
    }

    /*
     * Retourne l'âge
     */
    public int getAge() {
        return ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getInt(KEY_AGE, 0);
    }

    /*
     * Retourne le rôle
     */
    public String getRole() {
        return ctx.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getString(KEY_ROLE, null);
    }
}
