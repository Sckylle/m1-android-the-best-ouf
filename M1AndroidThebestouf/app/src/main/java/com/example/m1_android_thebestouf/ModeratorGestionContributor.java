package com.example.m1_android_thebestouf;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class ModeratorGestionContributor extends AppCompatActivity {

    private Button selectButton;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_moderator_gestion_contributor);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        int role = R.id.moderator;
        if(UserData.getInstance(this).getRole().equals("Administrateur")) {
            role = R.id.administrator;
        }
        selectButton = findViewById(role);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    public void head(View view) {
        Header.head(view, this);
    }
}
