package com.example.m1_android_thebestouf;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;

public class Header {

    private static final Header ourInstance = new Header();

    private Header() {

    }

    public static Header getInstance() {
        return ourInstance;
    }

    static void head(View v, Context context) {

        Intent intent = new Intent();

        switch (v.getId()) {

            case R.id.home1:
                intent.setClass(context, Home.class);
                context.startActivity(intent);
                break;

            case R.id.connect:
                intent.setClass(context, Connexion.class);
                context.startActivity(intent);
                break;

            case R.id.register:
                intent.setClass(context, Inscription.class);
                context.startActivity(intent);
                break;

            case R.id.challenge:
                intent.setClass(context, NewChallenge.class);
                context.startActivity(intent);
                break;

            case R.id.account:
                intent.setClass(context, Account.class);
                context.startActivity(intent);
                break;

            case R.id.contributor:
                intent.setClass(context, Contributor.class);
                context.startActivity(intent);
                break;
            case R.id.moderator:
                intent.setClass(context, Moderator.class);
                context.startActivity(intent);
                break;

            case R.id.administrator:
                intent.setClass(context, Administrator.class);
                context.startActivity(intent);
                break;
        }

    }

    public void changeHeader(View v, Context context) {

        Button contributor, moderator, administrator, connect, register, challenge, home, account;
        LinearLayout levelOne;

        levelOne = v.findViewById(R.id.level1);
        contributor = v.findViewById(R.id.contributor);
        moderator = v.findViewById(R.id.moderator);
        administrator = v.findViewById(R.id.administrator);
        home = v.findViewById(R.id.home1);
        connect = v.findViewById(R.id.connect);
        register = v.findViewById(R.id.register);
        challenge = v.findViewById(R.id.challenge);
        account = v.findViewById(R.id.account);

        // Si l'on n'est pas connecté
        if (!UserData.getInstance(context).isLoggedIn()) {
            levelOne.setWeightSum(3);
            contributor.setVisibility(View.GONE);
            moderator.setVisibility(View.GONE);
            administrator.setVisibility(View.GONE);
            home.setVisibility(View.VISIBLE);
            connect.setVisibility(View.VISIBLE);
            register.setVisibility(View.VISIBLE);
            challenge.setVisibility(View.GONE);
            account.setVisibility(View.GONE);
        } else if (UserData.getInstance(context).getRole().equals("Utilisateur")) {
            levelOne.setWeightSum(3);
            contributor.setVisibility(View.GONE);
            moderator.setVisibility(View.GONE);
            administrator.setVisibility(View.GONE);
            home.setVisibility(View.VISIBLE);
            connect.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            challenge.setVisibility(View.VISIBLE);
            account.setVisibility(View.VISIBLE);
        } else if (UserData.getInstance(context).getRole().equals("Contributeur")) {
            levelOne.setWeightSum(4);
            contributor.setVisibility(View.VISIBLE);
            moderator.setVisibility(View.GONE);
            administrator.setVisibility(View.GONE);
            home.setVisibility(View.VISIBLE);
            connect.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            challenge.setVisibility(View.VISIBLE);
            account.setVisibility(View.VISIBLE);
        } else if (UserData.getInstance(context).getRole().equals("Moderateur")) {
            levelOne.setWeightSum(4);
            contributor.setVisibility(View.GONE);
            moderator.setVisibility(View.VISIBLE);
            administrator.setVisibility(View.GONE);
            home.setVisibility(View.VISIBLE);
            connect.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            challenge.setVisibility(View.VISIBLE);
            account.setVisibility(View.VISIBLE);
        } else if (UserData.getInstance(context).getRole().equals("Administrateur")) {
            levelOne.setWeightSum(4);
            contributor.setVisibility(View.GONE);
            moderator.setVisibility(View.GONE);
            administrator.setVisibility(View.VISIBLE);
            home.setVisibility(View.VISIBLE);
            connect.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            challenge.setVisibility(View.VISIBLE);
            account.setVisibility(View.VISIBLE);
        }
    }
}
