package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewChallenge extends AppCompatActivity {

    private TextInputLayout textInputLayoutDescription;
    private Spinner spinnerCategorie;
    private Button buttonValide;
    private String spinnerValue;
    private Button selectButton;

    List<Defi> listeDefis = new ArrayList<Defi>();
    private RecyclerView recyclerView;

    private RecyclerViewAdapter recyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (!UserData.getInstance(getApplicationContext()).isLoggedIn()) {
            startActivity(new Intent(getApplicationContext(), Connexion.class));
            finish();
        }
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_new_challenge);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        spinnerCategorie = findViewById(R.id.spinnerCategorie);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.categorie, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategorie.setAdapter(adapter2);
        spinnerCategorie.setOnItemSelectedListener(new CategoriepinnerClass());

        textInputLayoutDescription = findViewById(R.id.descriptionInput);

        selectButton = findViewById(R.id.challenge);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);

        initRecyclerView();
        receivDefis();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    public void publieVideo(View view) {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            //Toast.makeText(getApplicationContext(), "OK!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, PostChallenge.class);
            startActivity(intent);
        } else {
            //Toast.makeText(getApplicationContext(), "pas OK!", Toast.LENGTH_SHORT).show();
        }
    }

    public void head(View view) {
        Header.head(view, this);
    }

    private boolean isNull(EditText text) {
        return text == null;
    }

    public void submitNewChallenge(View view) {

        String description = "";
        if (!isNull(textInputLayoutDescription.getEditText())) {
            description = textInputLayoutDescription.getEditText().getText().toString().trim();
        }

        int id = UserData.getInstance(getApplicationContext()).getId_user();
        String username = UserData.getInstance(getApplicationContext()).getUsername();
        String email = UserData.getInstance(getApplicationContext()).getEmail();
        String role = UserData.getInstance(getApplicationContext()).getRole();
        int age = UserData.getInstance(getApplicationContext()).getAge();
        Defi defi = new Defi(spinnerValue, description, new User(id, username, email, role, age));

        if (description.isEmpty()) {
            textInputLayoutDescription.setError(getString(R.string.emptyField));
        } else {
            DefiAdapter defiAdapter = new DefiAdapter(getApplicationContext());
            defiAdapter.sendDefi(defi);
            listeDefis.add(0,defi);
            recyclerViewAdapter.notifyDataSetChanged();
        }
        textInputLayoutDescription.getEditText().getText().clear();
    }

    class CategoriepinnerClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            spinnerValue = parent.getItemAtPosition(position).toString();
//            Toast.makeText(parent.getContext(), spinnerValue, Toast.LENGTH_SHORT).show();
//        if (position == 1) {
//            setContentView(R.layout.activity_main);
//        }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerViewAdapter = new RecyclerViewAdapter(this, listeDefis);
        RecyclerView.ItemDecoration divider = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void receivDefis () {

        RequestParams params = new RequestParams();
        params.put("Id_user", 0);
        BaseAdapter.post(Constantes.URL_GETCHALLENGE, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                DefiAdapter dA = new DefiAdapter(getApplicationContext());
                try {
                    JSONObject obj = response;
                    if (!obj.getBoolean("error")) {
                        JSONArray jsonArray = obj.getJSONArray("challenges");
                        User user;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Defi defi = dA.parseDefi(jsonArray.getJSONObject(i));
                            listeDefis.add(defi);
                            recyclerViewAdapter.notifyDataSetChanged();
                        }

                    } else {
                        //Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {

            }
        });
    }
}
