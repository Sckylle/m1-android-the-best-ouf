package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class Inscription extends AppCompatActivity {

    private TextInputLayout textInputLayoutUsername, textInputLayoutEmail, textInputLayoutPassword, textInputLayoutAge, textInputLayoutConfirmPassword;
    private Button buttonRegister;
    private Button selectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_inscription);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        if (UserData.getInstance(getApplicationContext()).isLoggedIn()) {
            startActivity(new Intent(getApplicationContext(), Home.class));
            finish();
        }

        selectButton = findViewById(R.id.register);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);

        textInputLayoutEmail = findViewById(R.id.emailInput);
        textInputLayoutUsername = findViewById(R.id.usernameInput);
        textInputLayoutPassword = findViewById(R.id.passwordInput);
        textInputLayoutAge = findViewById(R.id.ageInput);
        textInputLayoutConfirmPassword = findViewById(R.id.confirmPasswordInput);

        buttonRegister = findViewById(R.id.buttonRegister);


        /*
         * Evènement sur le focus du champ username
         */
        textInputLayoutUsername.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    textInputLayoutUsername.setErrorEnabled(false);
                    textInputLayoutUsername.setHelperTextEnabled(true);
                } else {
                    onFocusChangeUsername();
                }
            }
        });

        /*
         * Evènement sur le focus du champ email
         */
        textInputLayoutEmail.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    textInputLayoutEmail.setErrorEnabled(false);
                    textInputLayoutEmail.setHelperTextEnabled(true);
                } else {
                    onFocusChangeEmail();
                }
            }
        });

        /*
         * Evènement sur le focus du champ password
         */
        textInputLayoutPassword.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    textInputLayoutPassword.setErrorEnabled(false);
                    textInputLayoutPassword.setHelperTextEnabled(true);
                } else {
                    onFocusChangePassword();
                }
            }
        });

        /*
         * Evènement sur le focus du champ confirmPassword
         */
        textInputLayoutConfirmPassword.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    textInputLayoutConfirmPassword.setErrorEnabled(false);
                    textInputLayoutConfirmPassword.setHelperTextEnabled(true);
                } else {
                    onFocusChangeConfirmPassword();
                }
            }
        });

        /*
         * Evènement sur le focus du champ age
         */
        textInputLayoutAge.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    textInputLayoutAge.setErrorEnabled(false);
                    textInputLayoutAge.setHelperTextEnabled(true);
                } else {
                    onFocusChangeAge();
                }
            }
        });

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    /*
     * Retourne la chaîne contenue dans le textInputLayout
     */
    private String getString(TextInputLayout field) {
        return field.getEditText().getText().toString().trim();
    }

    /*
     * Enregistre un utilisateur dans la base de données
     */
    private void registerUser() {
        final String email = getString(textInputLayoutEmail);
        final String username = getString(textInputLayoutUsername);
        final String password = getString(textInputLayoutPassword);
        final String confirmPassword = getString(textInputLayoutConfirmPassword);
        final String age = getString(textInputLayoutAge);


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constantes.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("age", age);
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };


        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);


    }

    /*
     * Enregistre le nouvel utilisateur dans la base de données et le redirige vers la page principale
     * Affiche des erreurs sur les champs mal complétés si il y en a et n'inscrit pas l'utilisateur
     */
    public void onClick(View view) {
        final String email = getString(textInputLayoutEmail);
        final String username = getString(textInputLayoutUsername);
        final String password = getString(textInputLayoutPassword);

        ExpressionChecker expchk = new ExpressionChecker();
        boolean validUs = onFocusChangeUsername(), validPa = onFocusChangePassword(), validEm = onFocusChangeEmail(), validAge = onFocusChangeAge(), validCP = onFocusChangeConfirmPassword();
        boolean valid = validUs && validPa && validEm && validAge && validCP;

        if (expchk.checkRegistration(username, password, email) && valid) {
            registerUser();
            startActivity(new Intent(this, Home.class));
        } else {
            Toast.makeText(getApplicationContext(), "Certaines informations ne sont pas valides", Toast.LENGTH_LONG).show();
        }


    }


    /*
     * Affiche une erreur si le champ n'est pas complété correctement
     */
    private boolean onFocusChangeUsername() {
        final String username = getString(textInputLayoutUsername);
        ExpressionChecker expchk = new ExpressionChecker();
        boolean valid = false;
        if (username.isEmpty()) {
            textInputLayoutUsername.setError(getString(R.string.emptyField));
        } else if (username.length() < 6) {
            textInputLayoutUsername.setError(getString(R.string.username) + " " + getString(R.string.isTooShort));
        } else if (username.length() > 16) {
            textInputLayoutUsername.setError(getString(R.string.username) + " " + getString(R.string.isTooLong));
        } else if (!expchk.checkUsername(username)) {
            textInputLayoutUsername.setError(getString(R.string.username) + " " + getString(R.string.isntValid));
        } else {
            textInputLayoutUsername.setError("");
            valid = true;
        }

        return valid;
    }


    /*
     * Affiche une erreur si le champ n'est pas complété correctement
     */
    private boolean onFocusChangeEmail() {
        final String email = getString(textInputLayoutEmail);
        ExpressionChecker expchk = new ExpressionChecker();
        boolean valid = false;

        if (email.isEmpty()) {
            textInputLayoutEmail.setError(getString(R.string.emptyField));
        } else if (!expchk.checkEmail(email)) {
            textInputLayoutEmail.setError(getString(R.string.email) + " " + getString(R.string.isntValid));
        } else {
            textInputLayoutEmail.setError("");
            valid = true;
        }

        return valid;
    }


    /*
     * Affiche une erreur si le champ n'est pas complété correctement
     */
    private boolean onFocusChangePassword() {
        final String password = getString(textInputLayoutPassword);
        ExpressionChecker expchk = new ExpressionChecker();
        boolean valid = false;

        if (password.isEmpty()) {
            textInputLayoutPassword.setError(getString(R.string.emptyField));
        } else if (password.length() < 8) {
            textInputLayoutPassword.setError(getString(R.string.password) + " " + getString(R.string.isTooShort));
        } else if (!expchk.checkPassword(password)) {
            textInputLayoutPassword.setError(getString(R.string.password) + " " + getString(R.string.isntValid));
        } else {
            textInputLayoutPassword.setError("");
            valid = true;
        }

        return valid;
    }

    /*
     * Affiche une erreur si le champ n'est pas complété correctement
     */
    private boolean onFocusChangeConfirmPassword() {
        final String password = getString(textInputLayoutPassword);
        final String confirmPassword = getString(textInputLayoutConfirmPassword);
        ExpressionChecker expchk = new ExpressionChecker();
        boolean valid = false;

        if (confirmPassword.isEmpty()) {
            textInputLayoutConfirmPassword.setError(getString(R.string.emptyField));
        } else if (!expchk.checkConfirmPassword(password, confirmPassword)) {
            textInputLayoutConfirmPassword.setError(getString(R.string.identicalPasswords));
        } else {
            textInputLayoutConfirmPassword.setError("");
            valid = true;
        }

        return valid;
    }

    /*
     * Affiche une erreur si le champ n'est pas complété correctement
     */
    private boolean onFocusChangeAge() {
        final String age = getString(textInputLayoutAge);
        ExpressionChecker expchk = new ExpressionChecker();
        boolean valid = false;

        if (age.isEmpty()) {
            textInputLayoutAge.setError(getString(R.string.emptyField));
        } else if (!expchk.isInteger(age)) {
            textInputLayoutAge.setError(getString(R.string.notANumber));
        } else if (Integer.parseInt(age) < 18) {
            textInputLayoutAge.setError(getString(R.string.tooYoung));
        } else {
            textInputLayoutAge.setError("");
            valid = true;
        }

        return valid;
    }

    public void head(View view) {
        Header.head(view, this);
    }

}
