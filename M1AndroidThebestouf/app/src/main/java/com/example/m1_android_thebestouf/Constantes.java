package com.example.m1_android_thebestouf;

public class Constantes {

    public static final String ORDER_DESC = "DESC";
    private static final String ROOT_URL = "http://89.88.247.28:5555/";
    public static final String URL_REGISTER = ROOT_URL + "registerUser.php";
    public static final String URL_LOGIN = ROOT_URL + "userLogin.php";
    public static final String URL_NEWDEFI = ROOT_URL + "newChallenge.php";
    public static final String URL_GETCHALLENGE = ROOT_URL + "getChallenge.php";
    public static final String URL_UPLOADVIDEO = ROOT_URL + "uploadVideo.php";
    public static final String URL_GETVIDEOS = ROOT_URL + "getVideos.php";
    public static final String URL_LIKE = ROOT_URL + "likeVideo.php";
    public static final String URL_GETLIKEDISLIKE = ROOT_URL + "getLikeDislike.php";
    public static final String URL_GETLIKEDISLIKEVIDEO = ROOT_URL + "getLikeDislikeVideo.php";
    public static final String URL_GETSTATS = ROOT_URL + "getStats.php";
    public static final String URL_GETCOMMENTS = ROOT_URL + "getComments.php";
    public static final String URL_POSTCOMMENT = ROOT_URL + "postComment.php";
    public static final String URL_FOLLOW = ROOT_URL + "follow.php";
    public static final String URL_UNFOLLOW = ROOT_URL + "unfollow.php";
    public static final String URL_GETFOLLOWERS = ROOT_URL + "getFollowers.php";
    public static final String URL_GETFOLLOWING = ROOT_URL + "getFollowing.php";
    public static final String URL_ISFOLLOWING = ROOT_URL + "isFollowing.php";
    public static final String URL_GETVIDEOSBYCHALLENGE = ROOT_URL + "getVideosByChallenge.php";
}
