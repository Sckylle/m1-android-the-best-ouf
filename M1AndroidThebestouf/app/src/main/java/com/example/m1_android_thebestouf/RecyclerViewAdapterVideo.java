package com.example.m1_android_thebestouf;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapterVideo extends RecyclerView.Adapter<RecyclerViewAdapterVideo.ViewHolder> {

    private Context context;
    private List<Video> listeVideos;
    private boolean afficherChallenge;

    public RecyclerViewAdapterVideo(Context context, List<Video> listeVideos) {
        this.context = context;
        this.listeVideos = listeVideos;
        this.afficherChallenge = true;
    }

    public RecyclerViewAdapterVideo(Context context, List<Video> listeVideos, boolean afficherChallenge) {
        this.context = context;
        this.listeVideos = listeVideos;
        this.afficherChallenge = afficherChallenge;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_video, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if (afficherChallenge) {
            holder.challenge.setText(listeVideos.get(position).getDefi().getDescription());
        }else{
            holder.challenge.setVisibility(View.INVISIBLE);
            holder.challenge.setHeight(20);
        }
        holder.submitted.setText("Submitted by " + listeVideos.get(position).getUser().getUsername());
        holder.video.setVideoURI(Uri.parse(listeVideos.get(position).getUri()));
        holder.video.seekTo( 1 );


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent ();
                intent.setClass(context, VideoPlay.class);
                intent.putExtra("video", listeVideos.get(position));
                context.startActivity(intent);
            }
        });

        holder.challenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent ();
                intent.setClass(context, DefiActivity.class);
                intent.putExtra("defi", listeVideos.get(position).getDefi());
                context.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return listeVideos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView challenge;
        TextView submitted;
        RelativeLayout parentLayout;
        VideoView video;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Log.d("ViewHolder", "created");

            challenge = itemView.findViewById(R.id.challenge);
            submitted = itemView.findViewById(R.id.submitted);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            video = itemView.findViewById(R.id.videoView);
        }
    }
}
