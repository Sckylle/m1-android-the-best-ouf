package com.example.m1_android_thebestouf;

import android.app.Application;
import android.content.Context;

public class M1AndroidThebestouf extends Application {

    private static Context context;

    public static Context getAppContext() {
        return M1AndroidThebestouf.context;
    }

    public void onCreate() {
        super.onCreate();
        M1AndroidThebestouf.context = getApplicationContext();
    }
}
