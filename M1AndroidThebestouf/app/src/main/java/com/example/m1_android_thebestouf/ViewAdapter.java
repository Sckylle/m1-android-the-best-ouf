package com.example.m1_android_thebestouf;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewAdapter extends FragmentStateAdapter {

    private User user;

    public ViewAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
            Bundle args = new Bundle();
            if (user != null) {
                args.putParcelable("user", user);
            }


            switch (position) {
                case 0:
                    if (user != null) {
                        FragmentStats fragment = new FragmentStats();
                        fragment.setArguments(args);
                        return fragment;
                    }
                    else {
                        return new FragmentStats();
                    }
                case 1:
                    if (user != null) {
                        FragmentDefis fragment = new FragmentDefis();
                        fragment.setArguments(args);
                        return fragment;
                    }
                    else {
                        return new FragmentDefis();
                    }
                default:
                    if (user != null) {
                        FragmentVideos fragment = new FragmentVideos();
                        fragment.setArguments(args);
                        return fragment;
                    }
                    else {
                        return new FragmentVideos();
                    }
            }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public void receiveUser (User user) {
        this.user = user;
    }
}
