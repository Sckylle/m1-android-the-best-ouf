package com.example.m1_android_thebestouf;


import android.os.Parcel;
import android.os.Parcelable;

public class Defi implements Parcelable {

    public static final Creator<Defi> CREATOR = new Creator<Defi>() {
        @Override
        public Defi createFromParcel(Parcel in) {
            return new Defi(in);
        }

        @Override
        public Defi[] newArray(int size) {
            return new Defi[size];
        }
    };
    private User user;
    private int Id_challenge;
    private String categorie;
    private String description;

    public Defi(int Id_challenge, String categorie, String description, User user) {
        this.Id_challenge = Id_challenge;
        this.categorie = categorie;
        this.description = description;
        this.user = user;
    }

    public Defi(String categorie, String description, User user) {
        this.user = user;
        this.categorie = categorie;
        this.description = description;
    }

    protected Defi(Parcel in) {
        Id_challenge = in.readInt();
        categorie = in.readString();
        description = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_challenge() {
        return Id_challenge;
    }

    public void setId_challenge(int id_challenge) {
        Id_challenge = id_challenge;
    }

    public User getUser() {
        return user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id_challenge);
        dest.writeString(categorie);
        dest.writeString(description);
        dest.writeParcelable(user, flags);
    }
}
