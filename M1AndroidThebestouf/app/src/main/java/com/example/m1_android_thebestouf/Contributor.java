package com.example.m1_android_thebestouf;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Contributor extends AppCompatActivity {

    private Button selectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_contributor);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.contributeur, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new Contributor.ContributorSpinnerClass());

        int role = R.id.contributor;
        if(UserData.getInstance(this).getRole().equals("Moderateur")) {
            role = R.id.moderator;
        }
        else if(UserData.getInstance(this).getRole().equals("Administrateur")) {
            role = R.id.administrator;
        }
        selectButton = findViewById(role);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    public void head(View view) {
        Header.head(view, this);
    }

    class ContributorSpinnerClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String text = parent.getItemAtPosition(position).toString();
//            Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
//        if (position == 1) {
//            setContentView(R.layout.activity_main);
//        }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
