package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity {

    List<Video> listeVideos = new ArrayList<Video>();
    private RecyclerView recyclerView;

    private RecyclerViewAdapterVideo recyclerViewAdapterVideo;
    private Button selectButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_main);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);
        initRecyclerView();



        getVideos(0);

        selectButton = findViewById(R.id.home1);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    public void head(View view) {
        Header.head(view, this);
    }

    public void videoPlay(View view) {
        Intent intent = new Intent(this, VideoPlay.class);
        startActivity(intent);
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerViewAdapterVideo = new RecyclerViewAdapterVideo(this, listeVideos);
        RecyclerView.ItemDecoration divider = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
        recyclerView.setAdapter(recyclerViewAdapterVideo);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    /*
     * Retourne une liste de vidéos de la BDD
     * param context Le contexte
     * param order L'ordre des vidéos
     * param Id_user l'Id_user de l'utilisateur
     */
    public List<Video> getVideos (int Id_user) {

        RequestParams params = new RequestParams();
        params.put("order", "DESC");
        params.put("Id_user", Id_user);

        BaseAdapter.post(Constantes.URL_GETVIDEOS,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("videos");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject jresponse;
                JSONObject jdefi;
                JSONObject juserdefi;
                JSONObject juservideo;
                Defi defi;
                User userDefi;
                User userVideo;

                for(int i = 0; i < jsonArray.length(); i++){
                    try {
                        jresponse = jsonArray.getJSONObject(i);
                        jdefi = jresponse.getJSONObject("challenge");
                        juservideo = jresponse.getJSONObject("user");
                        juserdefi = jdefi.getJSONObject("user");

                        userDefi = new User(juserdefi.getInt("Id_user"), juserdefi.getString("username"), juserdefi.getString("email"), juserdefi.getString("role"), juserdefi.getInt("age"));
                        defi = new Defi(jdefi.getInt("Id_challenge"), jdefi.getString("categorie"), jdefi.getString("description"), userDefi);
                        userVideo = new User(juservideo.getInt("Id_user"), juservideo.getString("username"), juservideo.getString("email"), juservideo.getString("role"),juserdefi.getInt("age"));

                        Video video = new Video(userVideo, defi, jresponse.getString("pathname"), jresponse.getInt("nb_Likes"), jresponse.getInt("nb_Dislikes"), jresponse.getInt("Id_video"));
                        listeVideos.add(video);

                        recyclerViewAdapterVideo.notifyDataSetChanged();
                        //Toast.makeText(getContext(), jresponse.getString("pathname"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONArray response) {
//                Toast.makeText(getApplicationContext(), "Error : Request failed", Toast.LENGTH_LONG).show();
            }
        });

        return listeVideos;
    }


}