package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

public class Account extends AppCompatActivity {

    private TextView textViewUsername, textViewEmail, textFollowing, textViewRole, textFollowers;
    private User user;
    private ViewPager2 viewPager2;
    private TabLayout tabLayout;
    private Button selectButton;
    private Bundle bundle;
    private int follow, following;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_account);

        selectButton = findViewById(R.id.account);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        textViewUsername = findViewById(R.id.textViewUsername);
        textViewEmail = findViewById(R.id.textViewEmail);
        textViewRole = findViewById(R.id.textViewRole);
        textFollowing = findViewById(R.id.textFollowing);
        textFollowers =  findViewById(R.id.textFollowers);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            user = bundle.getParcelable("user");
            assert user != null;
            afficherCompte(user);
            getStats(user.getId_user());
        } else {
            afficherMonCompte();
            getStats(UserData.getInstance(this).getId_user());
        }

        viewPager2 = findViewById(R.id.view_pager);
        ViewAdapter viewAdapter = new ViewAdapter(this);
        viewAdapter.receiveUser(user);
        viewPager2.setAdapter(viewAdapter);


        tabLayout = findViewById(R.id.tabLayout);
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.OnConfigureTabCallback() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0:
                        tab.setText(getString(R.string.stats));
                        break;
                    case 1:
                        tab.setText(getString(R.string.challenges));
                        break;
                    case 2:
                        tab.setText(getString(R.string.videos));
                        break;
                }
            }
        });
        tabLayoutMediator.attach();

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    public void head(View view) {
        Header.head(view, this);
    }


    public void deconnection(View view) {
        UserData.getInstance(getApplicationContext()).logout();
        startActivity(new Intent(this, Home.class));
    }

    public void afficherMonCompte() {
        textViewUsername.setText(UserData.getInstance(this).getUsername());
        textViewEmail.setText("Mail : " + UserData.getInstance(this).getEmail());
        switch (UserData.getInstance(this).getRole()) {
            case "Contributeur":
                textViewRole.setText("Role : " + getString(R.string.roleCont));
                break;
            case "Moderateur":
                textViewRole.setText("Role : " + getString(R.string.roleMod));
                break;
            case "Administrateur":
                textViewRole.setText("Role : " + getString(R.string.roleAdm));
                break;
            default:
                textViewRole.setText("Role : " + getString(R.string.roleUsr));
                break;
        }
    }

    public void afficherCompte(User user) {
        textViewUsername.setText(user.getUsername());
        textViewEmail.setText(user.getEmail());
        textViewRole.setText(user.getRole());
    }

    public void getStats (int Id_user) {

        RequestParams params = new RequestParams();
        params.put("Id_user", Id_user);
        BaseAdapter.post(Constantes.URL_GETSTATS,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                try {

                    follow = response.getInt("nb_followers");
                    following = response.getInt("nb_following");

                    textFollowers.setText(getString(R.string.followers) + " : " + follow);
                    textFollowing.setText(getString(R.string.following) + " : " + following);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {

            }
        });
    }



    public void toSubscription(View view) {
        Intent intent = new Intent(this, Subscription.class);
        if (user != null) {
            intent.putExtra("user", user);
        }
        startActivity(intent);
    }
}
