package com.example.m1_android_thebestouf;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Gère l'envoi et la réception de défis avec la base de données.
 */
public class DefiAdapter {

    Context context;
    RecyclerViewAdapter recyclerViewAdapter;

    public DefiAdapter(Context context, RecyclerViewAdapter rA) {
        this.context = context;
        recyclerViewAdapter = rA;
    }

    public DefiAdapter(Context context) {
        this.context = context;

    }


    /*
     * Envoie le défi à la base de données
     */
    public void sendDefi(Defi defi) {
        final String categorie = defi.getCategorie();
        final String description = defi.getDescription();
        final String Id_user = Integer.toString(defi.getUser().getId_user());

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constantes.URL_NEWDEFI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("categorie", categorie);
                params.put("description", description);
                params.put("Id_user", Id_user);
                return params;
            }
        };


        RequestHandler.getInstance(context).addToRequestQueue(stringRequest);
    }

    /*
     * Récupère tous les défis postés par un utilisateur
     * param username Le nom de l'utilisateur
     * return Un tableau contenant les défis de l'utilisateur
     */
    public List<Defi> receiveDefis(final int Id_user) {
        final List<Defi> listeDefis = new ArrayList<Defi>();

        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST,
                Constantes.URL_GETCHALLENGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject obj = new JSONObject(response);

                            if (!obj.getBoolean("error")) {

                                JSONArray jsonArray = obj.getJSONArray("challenges");

                                User user;
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jresponse = jsonArray.getJSONObject(i);
                                    JSONObject juser = jresponse.getJSONObject("user");

                                    user = new User(juser.getInt("Id_user"), juser.getString("username"), juser.getString("email"), juser.getString("role"), juser.getInt("age"));
                                    Defi defi = new Defi(jresponse.getInt("Id_challenge"),
                                            jresponse.getString("categorie"),
                                            jresponse.getString("description"),
                                            user);
                                    //Toast.makeText(context, defi.getDescription(), Toast.LENGTH_SHORT).show();
                                    listeDefis.add(defi);
                                    recyclerViewAdapter.notifyDataSetChanged();

                                }

                            } else {
                                //Toast.makeText(context, obj.getString("message"), Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Id_user", String.valueOf(Id_user));
                return params;
            }
        };

        RequestHandler.getInstance(context).addToRequestQueue(jsonArrayRequest);

        return listeDefis;
    }

    public Defi parseDefi(JSONObject response) {
        User user;
        Defi defi = null;
        try {
            JSONObject juser = response.getJSONObject("user");

            user = new User(juser.getInt("Id_user"), juser.getString("username"), juser.getString("email"), juser.getString("role"), juser.getInt("age"));
            defi = new Defi(response.getInt("Id_challenge"),
                    response.getString("categorie"),
                    response.getString("description"),
                    user);
            //Toast.makeText(context, defi.getDescription(), Toast.LENGTH_SHORT).show();

            return defi;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return defi;
    }
}

