package com.example.m1_android_thebestouf;

import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class VideoAdapter {


    public void sendVideo(final Context context, Uri uri, Defi defi) {
        final String[] messageReturn = {"nothing"};

        RequestParams params = new RequestParams();

        String extension = context.getContentResolver().getType(uri);
        extension = extension.substring(extension.lastIndexOf("/") + 1);

        int Id_user = UserData.getInstance(context).getId_user();
        int Id_challenge = defi.getId_challenge();

        params.put("Id_user", Id_user);
        params.put("Id_challenge", Id_challenge);
        params.put("extension", extension);

        InputStream inputStream = null;
        try {
            inputStream = context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        params.put("fileToUpload", inputStream);

        BaseAdapter.post(Constantes.URL_UPLOADVIDEO, params, new JsonHttpResponseHandler() {


            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                try {
                    String rep = response.getString("message").equals("Already in BDD") ? "Success":response.getString("message");
                    Toast.makeText(context, rep, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
                //Toast.makeText(context, "Error : Request failed", Toast.LENGTH_LONG).show();
            }

        });

    }

    /*
     * Retourne la liste de toutes les vidéos de la BDD
     * param context Le contexte
     * param order L'ordre des vidéos
     */
    public List<Video> getAllvideos(final Context context, String order) {
        return getVideos(context, order, 0);
    }

    /*
     * Retourne la liste de toutes les vidéos de l'utilisateur
     * param context Le contexte
     * param order L'ordre des vidéos
     */
    public List<Video> getMyvideos(final Context context, String order) {
        return getVideos(context, order, UserData.getInstance(context).getId_user());
    }

    /*
     * Retourne une liste de vidéos de la BDD
     * param context Le contexte
     * param order L'ordre des vidéos
     * param Id_user l'Id_user de l'utilisateur
     */
    public List<Video> getVideos(final Context context, String order, int Id_user) {
        final List<Video> tabVideos = new ArrayList<Video>();
        RequestParams params = new RequestParams();
        params.put("order", order);
        params.put("Id_user", Id_user);

        BaseAdapter.post(Constantes.URL_GETVIDEOS, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("videos");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject jresponse;
                JSONObject jdefi;
                JSONObject juserdefi;
                JSONObject juservideo;
                Defi defi;
                User userDefi;
                User userVideo;

                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        jresponse = jsonArray.getJSONObject(i);
                        jdefi = jresponse.getJSONObject("challenge");
                        juservideo = jresponse.getJSONObject("user");
                        juserdefi = jdefi.getJSONObject("user");

                        userDefi = new User(juserdefi.getInt("Id_user"), juserdefi.getString("username"), juserdefi.getString("email"), juserdefi.getString("role"), juserdefi.getInt("age"));
                        defi = new Defi(jdefi.getInt("Id_challenge"), jdefi.getString("categorie"), jdefi.getString("description"), userDefi);
                        userVideo = new User(juservideo.getInt("Id_user"), juservideo.getString("username"), juservideo.getString("email"), juservideo.getString("role"), juserdefi.getInt("age"));

                        Video video = new Video(userVideo, defi, jresponse.getString("pathname"), jresponse.getInt("nb_Likes"), jresponse.getInt("nb_Dislikes"), jresponse.getInt("Id_video"));
                        tabVideos.add(video);
                        //Toast.makeText(context, jresponse.getString("pathname"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONArray response) {
                //Toast.makeText(context, "Error : Request failed", Toast.LENGTH_LONG).show();
            }
        });

        return tabVideos;
    }
}
