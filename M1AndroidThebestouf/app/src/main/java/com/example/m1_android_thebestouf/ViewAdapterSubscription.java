package com.example.m1_android_thebestouf;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewAdapterSubscription extends FragmentStateAdapter {

    private User user;

    public ViewAdapterSubscription(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Bundle args = new Bundle();
        if (user != null) {
            args.putParcelable("user", user);
        }


        if (position == 1) {
            if (user != null) {
                FragmentFollower fragment = new FragmentFollower();
                fragment.setArguments(args);
                return fragment;
            } else {
                return new FragmentFollower();
            }
        }
        if (user != null) {
            FragmentFollowing fragment = new FragmentFollowing();
            fragment.setArguments(args);
            return fragment;
        } else {
            return new FragmentFollowing();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public void receiveUser (User user) {
        this.user = user;
    }
}
