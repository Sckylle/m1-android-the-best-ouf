package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VideoPlay extends AppCompatActivity implements MediaPlayer.OnPreparedListener{

    private VideoView video;
    private ImageView rectimage;
    private ProgressBar progressBar;
    private MediaController mediaController;
    private int videoW, videoH;
    private RelativeLayout.LayoutParams paramsNotFullscreen;
    private RelativeLayout.LayoutParams paramsNotFullscreenBack;
    private RelativeLayout.LayoutParams paramsFullscreenBlack;
    private RelativeLayout.LayoutParams paramsFullscreen;
    private RelativeLayout.LayoutParams paramsNotFullscreenConteneur;
    private String videoURL;
    private Point fullRatio;
    private RelativeLayout videoFrame;
    private ScrollView scrollView;
    private Button selectButton;


    //Infos vidéo
    private Video vid;


    private Button buttonSubscription, signalementVideo;
    private ImageButton buttonLike, buttonDislike;

    private TextView usernameText, like, challengeText, dislike;

    private UserData usr;


    private int likes, dislikes;

    //Commentaires des videos
    List<Comment> listeComments = new ArrayList<Comment>();
    private RecyclerView recyclerView;
    private RecyclerViewAdapterComment recyclerViewAdapterComment;
    private TextInputLayout textInputLayout;
    private EditText textInputEdit;

    private boolean subscribeStatut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_video_play);

        selectButton = findViewById(R.id.home1);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        usernameText = findViewById(R.id.usernameText);
        dislike = findViewById(R.id.dislike);
        like = findViewById(R.id.like);
        challengeText = findViewById(R.id.challengeText);
        textInputLayout = findViewById(R.id.textInputLayoutComment);
        textInputEdit = findViewById(R.id.textInputEdit);
        buttonSubscription = findViewById(R.id.subscribeButton);
        buttonLike = findViewById(R.id.like_img);
        buttonDislike = findViewById(R.id.unlike_img);
        signalementVideo = findViewById(R.id.signalementVideo);

        Bundle bundle = getIntent().getExtras();

        /* Set l'URI de la video */
        if (bundle != null) {
            vid = bundle.getParcelable("video");
            try {
                assert vid != null;
                videoURL = vid.getUri();
            }catch (NullPointerException e){
                e.printStackTrace();
            }
            dislike.setText(String.valueOf(vid.getNb_Dislikes()));
            like.setText(String.valueOf(vid.getNb_Likes()));
            usernameText.setText(vid.getUser().getUsername());
            challengeText.setText(vid.getDefi().getDescription());
            getLikeDislikeVideo ();
        }else {
            videoURL = "http://89.88.247.28:5555/videos/44/4420.mp4";
        }

//        likeDislike.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                changeLikeDislikeState(v);
//            }
//        });
        rectimage = findViewById(R.id.rectimage);
        video = findViewById(R.id.videoLecteur);
        progressBar = findViewById(R.id.ProgressBar);
        videoFrame = findViewById(R.id.videoRelat);
        scrollView = findViewById(R.id.scroll);
        scrollView.setHorizontalScrollBarEnabled(false);


        usr = UserData.getInstance(this);
        if (!usr.isLoggedIn()) {
            LinearLayout yourCom = findViewById(R.id.yourComment);
            yourCom.setVisibility(View.GONE);
            buttonSubscription.setVisibility(View.GONE);
            signalementVideo.setVisibility(View.GONE);
        }

        /* Place le mediaController sur le cadre de la video */
        if (mediaController == null) {
            mediaController = new MediaController(VideoPlay.this);
            mediaController.setAnchorView(rectimage);
            video.setMediaController(mediaController);
        }

        /* quand on scroll, on fait disparaitre le mediaController */
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                mediaController.hide();
            }
        });

        /* On fixe les parametre plein écran / portrait */
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        paramsNotFullscreen = (RelativeLayout.LayoutParams) video.getLayoutParams();
        paramsNotFullscreenBack = (RelativeLayout.LayoutParams) rectimage.getLayoutParams();
        //paramsNotFullscreenConteneur = (RelativeLayout.LayoutParams) videoFrame.getLayoutParams();

        paramsFullscreenBlack = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsFullscreenBlack.setMargins(0, 0, 0, 0);
        paramsFullscreenBlack.addRule(RelativeLayout.CENTER_IN_PARENT);
        paramsFullscreen = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsFullscreen.setMargins(0, 0, 0, 0);
        paramsFullscreen.addRule(RelativeLayout.CENTER_IN_PARENT);

        video.setOnPreparedListener(this);

        /* Si il y a une coupure internet, on essaye de relancer le tous */
        video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
                if (what == MediaPlayer.MEDIA_ERROR_SERVER_DIED) {
                    video.stopPlayback();
                    progressBar.setVisibility(View.VISIBLE);
                    lancerVideo();
                }
                return true;
            }
        });
        isSubscribed();

        initRecyclerView();
        getComments ();
        lancerVideo();
    }

    /* On lance la video */
    private void lancerVideo() {
        video.setVisibility(View.VISIBLE);
        video.setVideoURI(Uri.parse(videoURL));
        video.start();
        //Log.d("tagg", "lancer video");
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    /* Si on tourne le téléphone en paysage, on passe la video en plein écran */
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        RelativeLayout header = findViewById(R.id.header);
        RelativeLayout nonVideo = findViewById(R.id.nonVideo);
        RelativeLayout root = findViewById(R.id.root);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

            //videoFrame.setLayoutParams(paramsFullscreenBlack);

            videoFrame.removeView(rectimage);
            videoFrame.removeView(video);
            root.addView(rectimage);
            root.addView(video);

            rectimage.setLayoutParams(paramsFullscreenBlack);

            fullRatio = ratio();
            paramsFullscreen.height = fullRatio.y;
            paramsFullscreen.width = fullRatio.x;

            video.setLayoutParams(paramsFullscreen);
            scrollView.getLayoutParams().height = paramsFullscreen.height;

            nonVideo.setVisibility(View.GONE);
            header.setVisibility(View.GONE);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

            root.removeView(rectimage);
            root.removeView(video);
            videoFrame.addView(rectimage);
            videoFrame.addView(video);

            video.setLayoutParams(paramsNotFullscreen);
            rectimage.setLayoutParams(paramsNotFullscreenBack);

            header.setVisibility(View.VISIBLE);
            nonVideo.setVisibility(View.VISIBLE);
        }
    }

    /* Renvoie un Point qui contient la taille que doit avoir la video a l'écran*/
    private Point ratio() {
        int fullWidth, fullHeight;

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        if (videoW > videoH) {

            fullWidth = size.x;
            fullHeight = size.x * videoH / videoW;
        } else {
            fullWidth = rectimage.getHeight() * videoW / videoH;
            fullHeight = rectimage.getHeight();
        }

        return (new Point(fullWidth, fullHeight));
    }

    public void head(View view) {
        Header.head(view, this);
    }

    /* Quand on clique sur signaler une video */
    public void signalVideo(View view) {

    }

    /* recuperation une information de like et dislike sur la bdd */
    public void getLikeDislikeVideo () {
        RequestParams params = new RequestParams();
        params.put("Id_video", vid.getId_video());
        BaseAdapter.post(Constantes.URL_GETLIKEDISLIKEVIDEO, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                try {
                    likes = response.getInt("likes");
                    dislikes = response.getInt("dislikes");
                    like.setText(String.valueOf(likes));
                    dislike.setText(String.valueOf(dislikes));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {

            }
        });
    }

    /* Envoie une information de like a la bdd */
    public void like (final View view) {
        if (usr.isLoggedIn()){
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.zoom);
            ImageButton btn;
            btn = findViewById(R.id.like_img);
            btn.startAnimation(animation);

            RequestParams params = new RequestParams();
            params.put("Id_user", UserData.getInstance(getApplicationContext()).getId_user());
            params.put("Id_video", vid.getId_video());
            params.put("value", 1);

            BaseAdapter.post(Constantes.URL_LIKE, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                    getLikeDislikeVideo ();
                }
                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
                }
            });
        }
    }

    /* Envoie une information de dislike a la bdd */
    public void dislike (final View view) {
        if (usr.isLoggedIn()) {

            Animation animation = AnimationUtils.loadAnimation(this, R.anim.zoom);
            ImageButton btn;
            btn = findViewById(R.id.unlike_img);
            btn.startAnimation(animation);

            RequestParams params = new RequestParams();
            params.put("Id_user", UserData.getInstance(getApplicationContext()).getId_user());
            params.put("Id_video", vid.getId_video());
            params.put("value", 0);

            BaseAdapter.post(Constantes.URL_LIKE, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                    getLikeDislikeVideo ();
                }
                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
                }
            });
        }
    }

    /* Lancer automatiquement a la récupérations de video */
    @Override
    public void onPrepared(MediaPlayer mp) {

        mediaController.setAnchorView(rectimage);

        progressBar.setVisibility(View.GONE);

        videoH = mp.getVideoHeight();
        videoW = mp.getVideoWidth();

        fullRatio = ratio();
        paramsNotFullscreen.height = fullRatio.y;
        paramsNotFullscreen.width = fullRatio.x;

        if (videoW > videoH) {
            paramsNotFullscreen.setMargins(0, 0, 0, (rectimage.getHeight() - videoH));
        } else {
            paramsNotFullscreen.setMargins(0, 0, 0, 0);
        }
        video.setLayoutParams(paramsNotFullscreen);
        mp.start();
    }

    /* Initialisation du recyclerView pour les commentaires */
    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerViewAdapterComment = new RecyclerViewAdapterComment(this, listeComments);
        RecyclerView.ItemDecoration divider = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
        recyclerView.setAdapter(recyclerViewAdapterComment);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    /*
     * Retourne une liste de vidéos de la BDD
     * param context Le contexte
     * param order L'ordre des vidéos
     * param Id_user l'Id_user de l'utilisateur
     */
    public List<Comment> getComments () {
        RequestParams params = new RequestParams();
        params.put("Id_video", vid.getId_video());

        BaseAdapter.post(Constantes.URL_GETCOMMENTS,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                JSONObject commentaire = null;
                JSONObject juser;
                try {
                    JSONArray jsonArray = response.getJSONArray("comments");
                    for(int i = 0; i < jsonArray.length(); i++) {
                        commentaire = jsonArray.getJSONObject(i);
                        juser = commentaire.getJSONObject("user");
                        User user = new User(juser.getInt("Id_user"), juser.getString("username"), juser.getString("email"), juser.getString("role"), juser.getInt("age"));
                        Comment comm = new Comment(user, commentaire.getInt("Id_video"), commentaire.getString("text"));


                        listeComments.add(comm);
                        recyclerViewAdapterComment.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
            }
        });

        return listeComments;
    }

    /* Envoie de commentaire a la bdd */
    public void postComment (View view) {
        final String text = textInputLayout.getEditText().getText().toString().trim();
        if (text.isEmpty()) {
            textInputLayout.setError(getString(R.string.emptyField));
        }else {
            RequestParams params = new RequestParams();
            params.put("Id_video", vid.getId_video());
            params.put("Id_user", UserData.getInstance(getApplicationContext()).getId_user());
            params.put("text", text);

            textInputEdit.getText().clear();

            BaseAdapter.post(Constantes.URL_POSTCOMMENT,params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {


                        Comment comm = new Comment(new User(getApplicationContext()), vid.getId_video(), text);
                        listeComments.add(0, comm);
                        recyclerViewAdapterComment.notifyDataSetChanged();
                        //Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT);

                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
                }
            });
        }
    }

    /* Récupere les information de la BDD sur l'abonnement de la personne */
    public void isSubscribed () {
        RequestParams params = new RequestParams();
        params.put("Id_following", vid.getUser().getId_user());
        params.put("Id_user", UserData.getInstance(getApplicationContext()).getId_user());

        BaseAdapter.post(Constantes.URL_ISFOLLOWING,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {

                try {
                    if (response.getBoolean("follow")) {
                        subscribeStatut = true;
                    }else {
                        subscribeStatut = false;
                    }
                    designButtonSubscription(subscribeStatut);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
            }
        });
    }

    /* Envoie les information d'abonnement a la bdd */
    public void subscribe (View view) {
        RequestParams params = new RequestParams();
        params.put("Id_follow", vid.getUser().getId_user());
        params.put("Id_user", UserData.getInstance(getApplicationContext()).getId_user());

        BaseAdapter.post(Constantes.URL_FOLLOW,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                isSubscribed ();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
            }
        });

    }

    /* Envoie les information de déabonnement a la bdd */
    public void unsubscribe (View view) {
        RequestParams params = new RequestParams();
        params.put("Id_follow", vid.getUser().getId_user());
        params.put("Id_user", UserData.getInstance(getApplicationContext()).getId_user());

        BaseAdapter.post(Constantes.URL_UNFOLLOW,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                isSubscribed ();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject response) {
            }
        });

    }

    /* Change le comportement au clique du bouton abonnement */
    public void changeStatut (View view) {

        if (subscribeStatut) {
            unsubscribe(view);
        } else {
            subscribe(view);
        }
    }

    /* Change le design de buttonSubscription en fonction dde l'abonnement  */
    public void designButtonSubscription(Boolean status){
        if (status) {
            buttonSubscription.setBackgroundResource(R.drawable.custom_button_empty_app);
            buttonSubscription.setText(R.string.unsubscribe);
        } else {
            buttonSubscription.setBackgroundResource(R.drawable.custom_button_app);
            buttonSubscription.setText(R.string.subscribe);
        }
    }

    public void versQuelquUn(View view) {
        Intent intent = new Intent(this, Account.class);
        intent.putExtra("user", vid.getUser());
        startActivity(intent);
    }

    public void versDefi(View view) {
        Intent intent = new Intent(this, DefiActivity.class);
        intent.putExtra("defi", vid.getDefi());
        startActivity(intent);
    }
}
