package com.example.m1_android_thebestouf;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapterUser extends RecyclerView.Adapter<RecyclerViewAdapterUser.ViewHolder> {

    private Context context;
    private List<User> listeUsers;

    public RecyclerViewAdapterUser(Context context, List<User> listeUsers) {
            this.context = context;
            this.listeUsers = listeUsers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
            }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
            holder.username.setText(listeUsers.get(position).getUsername());

            holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Intent intent = new Intent ();
                        intent.setClass(context, Account.class);
                        intent.putExtra("user", listeUsers.get(position));
                        context.startActivity(intent);
                        }
                        });

    }



    @Override
    public int getItemCount() {
            return listeUsers.size();
            }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView username;
        RelativeLayout parentLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Log.d("ViewHolder", "created");

            username = itemView.findViewById(R.id.username);
            parentLayout = itemView.findViewById(R.id.parent_layout);

        }
    }
}