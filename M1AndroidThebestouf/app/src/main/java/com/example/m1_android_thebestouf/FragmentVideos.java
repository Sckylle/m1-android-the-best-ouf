package com.example.m1_android_thebestouf;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentVideos  extends Fragment {

    private List<Video> listeVideos = new ArrayList<Video>();
    private RecyclerView recyclerView;
    private RecyclerViewAdapterVideo recyclerViewAdapterVideo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_videos, container, false);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        recyclerViewAdapterVideo = new RecyclerViewAdapterVideo(getActivity(), listeVideos);
        recyclerView.setAdapter(recyclerViewAdapterVideo);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Bundle bundle = getArguments();

        if (bundle != null) {
            User user = bundle.getParcelable("user");
            getVideos (user.getId_user());
        } else {
            getVideos(UserData.getInstance(getContext()).getId_user());
        }

        RecyclerView.ItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);

        return rootView;
    }

    /*
     * Retourne une liste de vidéos de la BDD
     * param context Le contexte
     * param order L'ordre des vidéos
     * param Id_user l'Id_user de l'utilisateur
     */
    public List<Video> getVideos (int Id_user) {

        RequestParams params = new RequestParams();
        params.put("order", "DESC");
        params.put("Id_user", Id_user);

        BaseAdapter.post(Constantes.URL_GETVIDEOS,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("videos");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject jresponse;
                JSONObject jdefi;
                JSONObject juserdefi;
                JSONObject juservideo;
                Defi defi;
                User userDefi;
                User userVideo;

                for(int i = 0; i < jsonArray.length(); i++){
                    try {
                        jresponse = jsonArray.getJSONObject(i);
                        jdefi = jresponse.getJSONObject("challenge");
                        juservideo = jresponse.getJSONObject("user");
                        juserdefi = jdefi.getJSONObject("user");

                        userDefi = new User(juserdefi.getInt("Id_user"), juserdefi.getString("username"), juserdefi.getString("email"), juserdefi.getString("role"), juserdefi.getInt("age"));
                        defi = new Defi(jdefi.getInt("Id_challenge"), jdefi.getString("categorie"), jdefi.getString("description"), userDefi);
                        userVideo = new User(juservideo.getInt("Id_user"), juservideo.getString("username"), juservideo.getString("email"), juservideo.getString("role"),juserdefi.getInt("age"));

                        Video video = new Video(userVideo, defi, jresponse.getString("pathname"), jresponse.getInt("nb_Likes"), jresponse.getInt("nb_Dislikes"), jresponse.getInt("Id_video"));
                        listeVideos.add(video);

                        recyclerViewAdapterVideo.notifyDataSetChanged();
                        //Toast.makeText(getContext(), jresponse.getString("pathname"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONArray response) {
                //Toast.makeText(getContext(), "Error : Request failed", Toast.LENGTH_LONG).show();
            }
        });

        return listeVideos;
    }
}