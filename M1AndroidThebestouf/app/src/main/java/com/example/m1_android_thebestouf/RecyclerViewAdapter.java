package com.example.m1_android_thebestouf;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Defi> listeDefis;


    public RecyclerViewAdapter(Context context, List<Defi> listeDefis) {
        Log.d("RecyclerViewAdapter", "created");
        this.context = context;
        this.listeDefis = listeDefis;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("RecyclerViewAdapter", "created");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listdefis, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d("RecyclerViewAdapter", "called");
        holder.description.setText(listeDefis.get(position).getDescription());
        holder.submitted.setText("Submitted by " + listeDefis.get(position).getUser().getUsername());
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, DefiActivity.class);
                intent.putExtra("defi", listeDefis.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.d("getItemcount", String.valueOf(listeDefis.size()));
        return listeDefis.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView description;
        TextView submitted;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Log.d("ViewHolder", "created");

            description = itemView.findViewById(R.id.description);
            submitted = itemView.findViewById(R.id.submitted);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
