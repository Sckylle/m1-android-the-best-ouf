package com.example.m1_android_thebestouf;

import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

public class CheckPermissions {

    private static final String REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";
    private static final String REQUEST_PERMISSION_INTERNET = "android.permission.INTERNET";
    private static final String REQUEST_PERMISSION_RECORD_AUDIO = "android.permission.RECORD_AUDIO";
    private static final String REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    private static final String REQUEST_PERMISSION_CAMERA = "android.permission.CAMERA";

    /*
     * Retourne Vrais si la permission WRITE_EXTERNAL_STORAGE est autoriser, sinon elle renvoie false
     * param context Le contexte
     */
    public boolean check_WRITE_EXTERNAL_STORAGE(Context context){
        if (context.checkCallingOrSelfPermission(REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            return false;
        }
        return true;
    }

    /*
     * Retourne Vrais si la permission REQUEST_PERMISSION_INTERNET est autoriser, sinon elle renvoie false
     * param context Le contexte
     */
    public boolean check_REQUEST_PERMISSION_INTERNET(Context context){
        if (context.checkCallingOrSelfPermission(REQUEST_PERMISSION_INTERNET) != PackageManager.PERMISSION_GRANTED){
            return false;
        }
        return true;
    }

    /*
     * Retourne Vrais si la permission REQUEST_PERMISSION_RECORD_AUDIO est autoriser, sinon elle renvoie false
     * param context Le contexte
     */
    public boolean check_PERMISSION_RECORD_AUDIO(Context context){
        if (context.checkCallingOrSelfPermission(REQUEST_PERMISSION_RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
            return false;
        }
        return true;
    }

    /*
     * Retourne Vrais si la permission REQUEST_PERMISSION_READ_EXTERNAL_STORAGE est autoriser, sinon elle renvoie false
     * param context Le contexte
     */
    public boolean check_READ_EXTERNAL_STORAGE(Context context){
        if (context.checkCallingOrSelfPermission(REQUEST_PERMISSION_READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            return false;
        }
        return true;
    }

    /*
     * Retourne Vrais si la permission REQUEST_PERMISSION_CAMERA est autoriser, sinon elle renvoie false
     * param context Le contexte
     */
    public boolean check_WRITE_EXTERNAL_CAMERA(Context context){
        if (context.checkCallingOrSelfPermission(REQUEST_PERMISSION_CAMERA) != PackageManager.PERMISSION_GRANTED){
            return false;
        }
        return true;
    }

    /*
     * Retourne Vrais si la permission toutes les permission sont accorder, sinon elle renvoie false et envoie un message a l'utilisateur
     * param context Le contexte
     */
    public boolean check_ALL(Context context){

        boolean booool = check_WRITE_EXTERNAL_STORAGE(context) && check_REQUEST_PERMISSION_INTERNET(context)
                && check_PERMISSION_RECORD_AUDIO(context) && check_READ_EXTERNAL_STORAGE(context)
                && check_WRITE_EXTERNAL_CAMERA(context);
//        Toast.makeText(context, String.valueOf(booool), Toast.LENGTH_SHORT).show();
        return booool;
    }

}
