package com.example.m1_android_thebestouf;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
    private int Id_user;
    private String username;
    private String email;
    private String role;
    //private String registration_date;
    private int age;

    public User(int id_user, String username, String email, String role, int age) {
        this.Id_user = id_user;
        this.username = username;
        this.email = email;
        this.role = role;
        this.age = age;

    }

    public User(Context context) {
        this.Id_user = UserData.getInstance(context).getId_user();
        this.username = UserData.getInstance(context).getUsername();
        this.email = UserData.getInstance(context).getEmail();
        this.role = UserData.getInstance(context).getRole();
        this.age = UserData.getInstance(context).getAge();
    }

    protected User(Parcel in) {
        Id_user = in.readInt();
        username = in.readString();
        email = in.readString();
        role = in.readString();
        age = in.readInt();
        //registration_date = in.readString();
    }

    public int getId_user() {
        return Id_user;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }

    public int getAge() {
        return age;
    }

    //public String getRegistration_date() {
    //return registration_date;
    //}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id_user);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(role);
        dest.writeInt(age);

    }

}
