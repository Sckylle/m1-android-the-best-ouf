package com.example.m1_android_thebestouf;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class Connexion extends AppCompatActivity {

    private EditText textInputLayoutUsername, textInputLayoutPassword;
    private Button buttonConnexion;
    private Button selectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_connexion);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        if (UserData.getInstance(getApplicationContext()).isLoggedIn()) {
            startActivity(new Intent(getApplicationContext(), Home.class));
            finish();
        }

        textInputLayoutUsername =  findViewById(R.id.usernameInput);
        textInputLayoutPassword =  findViewById(R.id.passwordInput);
        buttonConnexion =  findViewById(R.id.buttonConnexion);

        selectButton = findViewById(R.id.connect);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    //Header
    public void head(View view) {
        Header.head(view, this);
    }

    /*
     * Connecte l'utilisateur
     * Affiche des erreurs si l'identifiant ou le le mot de passe sont faux
     */
    public void userLogin() {
        final String username = textInputLayoutUsername.getText().toString().trim();
        final String password = textInputLayoutPassword.getText().toString().trim();


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constantes.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject obj = new JSONObject(response);
                            if (!obj.getBoolean("error")) {

                                //Récupération des informations utilisateur
                                UserData.getInstance(getApplicationContext())
                                        .userLogin(
                                                obj.getInt("Id_user"),
                                                obj.getString("username"),
                                                obj.getString("email"),
                                                obj.getInt("age"),
                                                obj.getString("role")
                                        );

                                Toast.makeText(getApplicationContext(), "Logged in", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(), Home.class));
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void onClickLogin(View view) {
        if (view == buttonConnexion) {
            userLogin();


        }
    }
}
