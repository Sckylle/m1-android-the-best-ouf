package com.example.m1_android_thebestouf;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapterComment extends RecyclerView.Adapter<RecyclerViewAdapterComment.ViewHolder> {

private Context context;
private List<Comment> listeComments;

public RecyclerViewAdapterComment(Context context, List<Comment> listeComments) {
        this.context = context;
        this.listeComments = listeComments;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_comment, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.username.setText(listeComments.get(position).getUser().getUsername());
        holder.text.setText(listeComments.get(position).getText());
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        Intent intent = new Intent ();
        intent.setClass(context, Account.class);
        intent.putExtra("user", listeComments.get(position).getUser());
        context.startActivity(intent);
        }
        });

        }



@Override
public int getItemCount() {
        return listeComments.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{

    TextView username, text;
    RelativeLayout parentLayout;


    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        Log.d("ViewHolder", "created");

        text = itemView.findViewById(R.id.text);
        username = itemView.findViewById(R.id.submitted);
        parentLayout = itemView.findViewById(R.id.parent_layout);

    }
}
}