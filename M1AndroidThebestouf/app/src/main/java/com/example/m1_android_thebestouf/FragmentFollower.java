package com.example.m1_android_thebestouf;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentFollower extends Fragment {
    private List<User> listeUsers = new ArrayList<User>();
    private RecyclerView recyclerView;
    private RecyclerViewAdapterUser recyclerViewAdapterUser;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_defis, container, false);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        recyclerViewAdapterUser = new RecyclerViewAdapterUser(getActivity(), listeUsers);
        recyclerView.setAdapter(recyclerViewAdapterUser);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Bundle bundle = getArguments();

        if (bundle != null) {
            User user = bundle.getParcelable("user");
            getFollowers (user.getId_user());
        } else {
            getFollowers(UserData.getInstance(getContext()).getId_user());
        }

        RecyclerView.ItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
        return rootView;
    }

    /*
     * Retourne une liste de vidéos de la BDD
     * param context Le contexte
     * param order L'ordre des vidéos
     * param Id_user l'Id_user de l'utilisateur
     */
    public void getFollowers (int Id_user) {

        RequestParams params = new RequestParams();
        params.put("Id_user", Id_user);

        BaseAdapter.post(Constantes.URL_GETFOLLOWERS,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                JSONArray jsonArray = null;
                try {
                    if (!response.getBoolean("error")) {
                        jsonArray = response.getJSONArray("users");
                        JSONObject juser;
                        User user;

                        for (int i = 0; i < jsonArray.length(); i++) {
                            juser = jsonArray.getJSONObject(i);
                            user = new User(juser.getInt("Id_user"), juser.getString("username"), juser.getString("email"), juser.getString("role"),juser.getInt("age"));
                            listeUsers.add(user);
                            recyclerViewAdapterUser.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONArray response) {
                Toast.makeText(getContext(), "Error : Request failed", Toast.LENGTH_LONG).show();
            }
        });

    }
}
