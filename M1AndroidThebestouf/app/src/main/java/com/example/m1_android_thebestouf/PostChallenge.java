package com.example.m1_android_thebestouf;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class PostChallenge extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    private static int VIDEO_REQUEST = 101;
    private Uri uri = null;
    private VideoView video;
    private ImageView rectimage;
    private RelativeLayout.LayoutParams paramsNotFullscreen;
    private RelativeLayout.LayoutParams paramsNotFullscreenBack;
    private RelativeLayout.LayoutParams paramsFullscreenBlack;
    private RelativeLayout.LayoutParams paramsFullscreen;
    private RelativeLayout.LayoutParams paramsNotFullscreenConteneur;
    private Button send;
    private Point fullRatio;
    private MediaController mediaController;
    private int videoW, videoH;
    private Defi defi;
    private RelativeLayout videoFrame;
    private TextView textcentral;
    private Button selectButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
        setContentView(R.layout.activity_post_challenge);

        Header head = Header.getInstance();
        head.changeHeader(this.findViewById(android.R.id.content), this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            defi = bundle.getParcelable("defi");
        }

        int role = R.id.challenge;
        if (!UserData.getInstance(this).isLoggedIn()) {
            role = R.id.home1;
        }
        selectButton = findViewById(role);
        selectButton.setBackgroundResource(R.drawable.custom_button_select);

        rectimage = findViewById(R.id.rectimage);
        video = findViewById(R.id.videoLecteur);
        send = findViewById(R.id.envoie);
        videoFrame = findViewById(R.id.videoRelat);
        videoFrame.setVisibility(View.GONE);
        send.setVisibility(View.GONE);
        textcentral = findViewById(R.id.textViewCentrale);
        textcentral.setVisibility(View.GONE);

        if (mediaController == null) {
            mediaController = new MediaController(PostChallenge.this);
            mediaController.setAnchorView(rectimage);
            video.setMediaController(mediaController);
        }
        paramsNotFullscreen = (RelativeLayout.LayoutParams) video.getLayoutParams();
        paramsNotFullscreenBack = (RelativeLayout.LayoutParams) rectimage.getLayoutParams();
        paramsNotFullscreenConteneur = (RelativeLayout.LayoutParams) videoFrame.getLayoutParams();

        paramsFullscreenBlack = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        paramsFullscreenBlack.setMargins(0, 0, 0, 0);
        paramsFullscreenBlack.addRule(RelativeLayout.CENTER_IN_PARENT);
        paramsFullscreen = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        paramsFullscreen.setMargins(0, 0, 0, 0);
        paramsFullscreen.addRule(RelativeLayout.CENTER_IN_PARENT);

        video.setOnPreparedListener(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_no_change, R.anim.slide_no_change);
    }

    public void head(View view) {
        Header.head(view, this);
    }

    public void captureVideo(View view) {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, VIDEO_REQUEST);
        }
    }

    /* Lancement de la video */
    private void lanceVideo() {

        if (uri != null) {
            videoFrame.setVisibility(View.VISIBLE);
            send.setVisibility(View.VISIBLE);
            textcentral.setVisibility(View.VISIBLE);
            video.setVideoURI(uri);
            video.start();
        }
    }

    /* Retour de l'activiter de prise de video */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);

        Log.d("ScreenH", "act result : " + requestCode + " " + resultCode);
        if (requestCode == VIDEO_REQUEST && resultCode == RESULT_OK) {
            try {
                assert intent != null;
                uri = intent.getData();
                lanceVideo();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            send.setVisibility(View.GONE);
            videoFrame.setVisibility(View.GONE);
            textcentral.setVisibility(View.GONE);
        }
    }

    /* Si on tourne le téléphone en paysage, on passe la video en plein écran */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Button capture = findViewById(R.id.capture);
        RelativeLayout header = findViewById(R.id.header);

        if (uri != null) {
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

                View decorView = getWindow().getDecorView();
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

                videoFrame.setLayoutParams(paramsFullscreenBlack);
                rectimage.setLayoutParams(paramsFullscreenBlack);

                fullRatio = ratio();
                paramsFullscreen.height = fullRatio.y;
                paramsFullscreen.width = fullRatio.x;

                video.setLayoutParams(paramsFullscreen);

                capture.setVisibility(View.GONE);
                send.setVisibility(View.GONE);
                header.setVisibility(View.GONE);

            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

                videoFrame.setLayoutParams(paramsNotFullscreenConteneur);

                video.setLayoutParams(paramsNotFullscreen);
                rectimage.setLayoutParams(paramsNotFullscreenBack);
                capture.setVisibility(View.VISIBLE);
                send.setVisibility(View.VISIBLE);
                header.setVisibility(View.VISIBLE);
            }
        }
    }

    /* Au clique sur send on envoie la video a la bdd */
    public void envoieDB(View view) {
        VideoAdapter vA = new VideoAdapter();
        vA.sendVideo(getApplicationContext(), uri, defi);

        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    /* Renvoie un Point qui contient la taille que doit avoir la video a l'écran*/
    private Point ratio() {
        int fullWidth, fullHeight;

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        Log.d("tagoulet", String.valueOf(size.x));
        Log.d("tagoulet", String.valueOf(size.y));
        Log.d("tagoulet", " ");

        if (videoW > videoH) {

            fullWidth = size.x;
            fullHeight = size.x * videoH / videoW;
        } else {
            fullWidth = rectimage.getHeight() * videoW / videoH;
            fullHeight = rectimage.getHeight();
        }

        return (new Point(fullWidth, fullHeight));
    }

    public void onClick(View view) {
    }

    /* Lancer automatiquement a la récupérations de video */
    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaController.setAnchorView(rectimage);

//                        Log.d("ScreenH", "onVideoSizeChanged : " );
        videoH = mediaPlayer.getVideoHeight();
        videoW = mediaPlayer.getVideoWidth();

        fullRatio = ratio();
        paramsNotFullscreen.height = fullRatio.y;
        paramsNotFullscreen.width = fullRatio.x;

        if (videoW > videoH) {
            paramsNotFullscreen.setMargins(0, 0, 0, (rectimage.getHeight() - videoH));
        } else {
            paramsNotFullscreen.setMargins(0, 0, 0, 0);
        }
        video.setLayoutParams(paramsNotFullscreen);
        mediaPlayer.start();
    }
}